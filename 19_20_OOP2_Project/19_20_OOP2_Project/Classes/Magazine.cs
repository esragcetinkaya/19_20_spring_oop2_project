﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19_20_OOP2_Project.Classes
{
    /*! \class Magazine
      * \brief It is Magazine class.
      * \details it is derived from Product class
      * \author Aleyna Akcinar
      */
    class Magazine : Product
    {
        public string issue { get; set; }
        public static int TotalMagazine { get; set; } = 0;
        public int Stock { get; set; }
        public TypeofProduct type { get; set; }

        public Magazine(int id, string name, float price, string Issue, int stock) : base(name, price, "Magazine", stock)
        {
            setID(id);
            issue = Issue;
            type = Type;
            Stock = stock;
        }
        /*! \fn override void Print()
         *  \brief A void function.
         *  \details It is overrided from Product class.It is used to print information of Magazine. 
         *  \return void
        */

        public override string printProperties()
        {
            //base.Print();
            //Console.Write(" | " + type + " | " + issue + Environment.NewLine);
            return "Name: " + base.getName() + Environment.NewLine +
                    "Price: " + base.getPrice() + Environment.NewLine +
                    "Type: " + getType() + Environment.NewLine +
                    "Stock: " + base.getStockCount() + Environment.NewLine +
                    "ID: " + TotalMagazine + Environment.NewLine;

        }
        public string printCSV()
        {
            return getID() + "," + getName() + "," + "Magazine" + "," + getPrice() + "," + issue + "," + Stock;
        }

        public int getTotalMagazine()
        {
            return TotalMagazine;
        }

        public void setTotalMagazine(int i)
        {
            TotalMagazine = i;
        }
    }
}
