﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace _19_20_OOP2_Project.Classes
{
    public class Product
    {

        string Name;
        int ID;
        float Price;
        int StockCount;
        internal TypeofProduct Type;
      
        public enum TypeofProduct { Magazine, MusicCD, Book }
        public Product(string _Name, float _Price, string _Type, int _StockCount)
        {
            this.Name = _Name;
            this.Price = _Price;
            Type = (TypeofProduct)Enum.Parse(typeof(TypeofProduct), _Type);
            this.StockCount = _StockCount;
            setID();
        }
        public virtual void setID()
        {
            long i = 1;
            foreach (byte b in Guid.NewGuid().ToByteArray())
            {
                i *= ((int)b + 1);
            }
            string number = String.Format("{0:d9}", (DateTime.Now.Ticks / 10) % 1000000000);
            this.ID = int.Parse(number);
        }
        public virtual void setID(int id){ ID = id; }
        public virtual void setName(string name) { this.Name = name; }
        public virtual void setPrice(int price) { this.Price = price; }
        public virtual void setStockCount(int count) { this.StockCount = count; }
        public virtual void setType(TypeofProduct type) { this.Type = type; }
        public virtual int getID() { return this.ID; }
        public virtual string getType() { return this.Type.ToString(); }
        public virtual string getName() { return this.Name; }
        public virtual int getStockCount() { return this.StockCount; }
        public virtual float getPrice() { return this.Price; }
        public virtual string printProperties()
        {
            return "Name: " + this.Name + Environment.NewLine +
                  "Id:" + this.ID + Environment.NewLine +
                 "Price: " + this.Price + Environment.NewLine +
                 "Stock Count:" + this.StockCount + Environment.NewLine;
        }

    }
}
