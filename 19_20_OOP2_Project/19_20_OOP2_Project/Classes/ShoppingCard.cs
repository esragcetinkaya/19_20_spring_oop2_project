﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Drawing.Printing;
using System.Security.Policy;
using System.Data;
using System.Runtime.InteropServices.ComTypes;
using System.Diagnostics;
using _19_20_OOP2_Project.Forms;

namespace _19_20_OOP2_Project.Classes
{
    public class ShoppingCard
    {
        private float paymentAmount;
        public string customer;
        string paymentType;
        public ItemToPurchase newItem;
        int quantity;
        public ShoppingCard() { }
        public ShoppingCard(string _CustomerID, string _PaymentType, int productID,int quantity)
        {
            customer = _CustomerID;
            paymentType = _PaymentType;
            this.quantity = quantity;
            foreach (Product item in main.ListOfProduct)
            {
                if (item.getID() ==productID)
                {
                    ItemToPurchase newItem = new ItemToPurchase(item.getID(), quantity);
                    this.newItem = newItem;
                }
            }
        }
        public void setPaymentAmount(){}
        //Return total amount of payment
        public float getPaymentAmount() { return paymentAmount; }
        //Shows the items currently in the cart
        public void addProduct(ItemToPurchase newItem)
        {
            //DATABASE GEREKLİ
        }
        //Remove an item from the shopping card
        public void removeProduct(ItemToPurchase newItem)
        {
            //DATABASE GEREKLİ
        }
        //Perform the payment for the items in the cart and send invoice to custumer’s email.
        public void placeOrder() { }
        //Cancel the order
        public void cancelOrder() { }
        public void sendInvoicebySMS() { }
        public void sendInvoidcebyEmail() { }

    }
}
