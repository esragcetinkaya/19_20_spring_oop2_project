﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using _19_20_OOP2_Project.Classes;

namespace _19_20_OOP2_Project.Classes
{
    public static class GetPathCsv
    {

        public static string getPath(string a)
        {
            var outPutDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
            // debuf file'in yolu
            var csvPath = Path.Combine(outPutDirectory, a + ".csv");
            // csv dosyasının directory'si
            string csv_path = new Uri(csvPath).LocalPath;
            // path'de bu isimde dosya yoksa aynı isimde doysa oluşturur.
            if (File.Exists(csv_path) == false)
            {
                File.Create(a + ".csv").Close();
            }
            return csv_path;
        }
        public static bool pathExists(string path)
        {
            var outPutDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
            // debuf file'in yolu
            var csvPath = Path.Combine(outPutDirectory, path + ".csv");
            // csv dosyasının directory'si
            string csv_path = new Uri(csvPath).LocalPath;
            // path'de bu isimde dosya yoksa aynı isimde doysa oluşturur.
            if (File.Exists(csv_path) == false)
            {
                return false;
            }
            return true;
        }

    }
}
