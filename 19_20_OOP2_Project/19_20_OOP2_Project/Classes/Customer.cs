﻿/*! A Customer Class */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace _19_20_OOP2_Project.Classes
{
    /****************************************
     *  \class  Customer
     *  \brief  Customer class 
     *  \author Emin Kocan
     *  \date   25/05/2020
     *  JAVADOC_AUTOBRIEF is ON
     ***************************************/
    public class Customer
    {
        private string name;        /**< Name field */
        private string username;    /**< Username field */
        private string password;    /**< Password field */
        private string id;          /**< ID field */
        private string email;       /**< Email field */
        private string adress;      /**< Adress field */
        private string userrole;      /**< Userrole field */
        private ShoppingCard bucket;
        /**
        * Constructor using all required fields.
        *
        * @param name_ stands for a name
        * @param username_ stands for an username
        * @param password_ stands for a password
        * @param id_ stands for an id
        * @param email_ stands for an email
        * @param adress_ stands for an adress
        */
        public Customer(string username_, string password_, string name_, string email_, string address_, string id_, string userrole_,ShoppingCard _bucket)
        {
            Name = name_;
            Username = username_;
            Password = password_;
            ID = id_;
            Email = email_;
            Adress = address_;
            userrole = userrole_;
            this.bucket = _bucket;
        }
        public Customer(string username_, string password_, string name_, string email_, string address_, string id_, string userrole_)
        {
            Name = name_;
            Username = username_;
            Password = password_;
            ID = id_;
            Email = email_;
            Adress = address_;
            userrole = userrole_;
        }

        public Customer()
        {
        }

        /**
        * Get/set function for username var.
        * 
        */
        public string Username { get => username; set => username = value; }
        /**
        * Get/set function for name var.
        * 
        */
        public string Name { get => name; set => name = value; }
        /**
        * Get/set function for password var.
        * 
        */
        public string Password { get => password; set => password = value; }
        /**
        * Get/set function for ID var.
        * 
        */
        public string ID { get => id; set => id = value; }
        /**
        * Get/set function for email var.
        * 
        */
        public string Email { get => email; set => email = value; }
        /**
        * Get/set function for adress var.
        * 
        */
        public string Adress { get => adress; set => adress = value; }
        /**
        * Get/set function for userrole var.
        * 
        */
        public string Userrole { get => userrole; set => userrole = value; }
        /**
        * Function to compute 256-Hash used to store passsword variable. Will probably be of use when handling logins.
        *
        * @param text stands for any text.
        */

        public string[] printCustomerDetails()
        {

            string[] details = new string[7];
            details[0] = Username;
            details[1] = Password;
            details[2] = Name;
            details[3] = Email;
            details[4] = Adress;
            details[5] = ID;
            details[6] = Userrole;
            return details;
        }
        /**
        * Not implemented yet. To-do.
        *
        */
        public void saveCustomer() { }
        /**
        * Not implemented yet. To-do.
        *
        */
        public void printCustomerPurchases() { }
    }
}
