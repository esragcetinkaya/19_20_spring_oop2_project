﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace _19_20_OOP2_Project.Classes
{
    /**********************************************************************************************//**
     * \class   Book:Product
     *
     * \brief   A book.
     *
     * \author  
     * \date    
     ***************************************************************************************************/

    class Book : Product
    {

        /** \brief   The ısbn */
        private string ISBN;

        /** \brief   The author */
        private string Author;

        /** \brief   The publisher */
        private string Publisher;

        /** \brief   The page */
        private int Page;

        /**********************************************************************************************//**
         * \fn  public Book(int _Id,string _Name,float _Price,string _Type,int _Quantity, string _ISBN,string _Author,string _Publisher,int _Page) : base(_Id, _Name, _Price,_Type,_Quantity)
         *
         * \brief   Constructor of Class
         * \param   _Id         The ıd.
         * \param   _Name       The name.
         * \param   _Price      The price.
         * \param   _Type       The type.
         * \param   _Quantity   The quantity.
         * \param   _ISBN       The ısbn.
         * \param   _Author     The author.
         * \param   _Publisher  The publisher.
         * \param   _Page       The page.
         **************************************************************************************************/
        /*
       public Book(string _Name, float _Price, TypeofProduct _Type, int _StockCount,
           string _ISBN, string _Author, string _Publisher, int _Page) : base(_Name, _Price, _Type, _StockCount)
       {
           ISBN = _ISBN;
           Author = _Author;
           Publisher = _Publisher;
           Page = _Page;
       }
       */
        public Book(string id, string name, float price, int stock, string isbn, string author, string publisher, int page) : base(name, price, "Book", stock)
        {
            this.setID(Convert.ToInt32(id));
            ISBN = isbn;
            Author = author;
            Publisher = publisher;
            Page = page;


        }

        /**********************************************************************************************//**
         * \fn  public override int getQuantity()
         *
         * \brief   Gets the quantity
         * \return  The quantity.
         **************************************************************************************************/

        public override int getStockCount() { return base.getStockCount(); }

        /**********************************************************************************************//**
         * \fn  public override int getID()
         *
         * \brief   Gets the ıd
         * \return  The ıd.
         **************************************************************************************************/

        public override int getID() { return base.getID(); }

        /**********************************************************************************************//**
         * \fn  public override void setQuantity(int X)
         *
         * \brief   Sets a quantity
         * \param   X   The X coordinate.
         **************************************************************************************************/

        public override void setStockCount(int X) { base.setStockCount(X); }

        /**********************************************************************************************//**
         * \fn  public override float getPrice()
         *
         * \brief   Gets the price
         *      
         * \return  The price.
         **************************************************************************************************/

        public override float getPrice() { return base.getPrice(); }

        /**********************************************************************************************//**
         * \fn  public override string getName()
         *
         * \brief   Gets the name
         * \return  The name.
         **************************************************************************************************/

        public override string getName() { return base.getName(); }

        /**********************************************************************************************//**
         * \fn  public string getType()
         *
         * \brief   Gets the type
         * \return  The type.
         **************************************************************************************************/

        public override string getType() { return "Book"; }

        /**********************************************************************************************//**
         * \fn  public override string printProperties()
         *
         * \brief   Print properties
         * \return  A string.
         **************************************************************************************************/
        public string getISBN()
        {
            return ISBN;
        }
        public string getAuthor()
        {
            return Author;
        }
        public int getPage()
        {
            return Page;
        }
        public string getPublisher()
        {
            return Publisher;
        }

        public override string printProperties()
        {
            return "Name: " + base.getName() + Environment.NewLine +
                "Price: " + base.getPrice() + Environment.NewLine +
                "Type: " + "Book" + Environment.NewLine +
                "Quantity: " + base.getStockCount() + Environment.NewLine +
                "ISBN: " + ISBN + Environment.NewLine +
                "Author:  " + Author + Environment.NewLine +
                "Publisher: " + Publisher + Environment.NewLine +
                "Page: " + Page + Environment.NewLine;
        }
        public string printCSV()
        {
            return base.getID() + "," + base.getName() + "," + base.getType() + "," + base.getPrice() + "," + base.getStockCount() + "," + getISBN() + "," + getAuthor() + "," + getPublisher() + "," + getPage();
        }
    }
}
