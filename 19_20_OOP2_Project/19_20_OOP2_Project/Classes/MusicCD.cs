﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19_20_OOP2_Project.Classes
{
    /**********************************************************************************************//**
     * \class   MusicCD
     *
     * \brief   A music cd.
     *
     * \author  
     * \date    
     **************************************************************************************************/
    class MusicCD : Product
    {
        /** \brief   The singer */
        private string Singer;

        /** \brief   Type of the mcd */
        private string MCD_Type;

        /**********************************************************************************************//**
         * \fn  public MusicCD(int _Id, string _Name, float _Price, string _Type, int _Quantity, string _Singer,string MCD_type) : base(_Id, _Name, _Price,_Type,_Quantity)
         *
         * \brief   Constructor
         * \param   _Id         The ıd.
         * \param   _Name       The name.
         * \param   _Price      The price.
         * \param   _Type       The type.
         * \param   _Quantity   The quantity.
         * \param   _Singer     The singer.
         * \param   MCD_type    Type of the mcd.
         **************************************************************************************************/

        public MusicCD(int _Id, string _Name, float _Price, int _Quantity, string _Singer, string MCD_type) : base(_Name, _Price, "MusicCD", _Quantity)
        {
            setID(_Id);
            Singer = _Singer;
            MCD_Type = MCD_type;
        }

        /**********************************************************************************************//**
         * \fn  public string getType()
         *
         * \brief   Gets the type
         * \return  The type.
         **************************************************************************************************/
        public string printCSV()
        {
            return getID() + "," + getName() + "," + getType() + "," + getPrice() + "," + getStockCount() + "," + getSinger() + "," + getMCDType();
        }
        public override string getType() { return "MusicCD"; }

        /**********************************************************************************************//**
         * \fn  public override string printProperties()
         *
         * \brief   Print properties
         * \return  A string.
         **************************************************************************************************/

        public override string printProperties()
        {
            return "Name: " + base.getName() + Environment.NewLine +
                "Price: " + base.getPrice() + Environment.NewLine +
                "Type: " + MCD_Type + Environment.NewLine +
                "Quantity: " + base.getStockCount() + Environment.NewLine +
                "Singer: " + Singer + Environment.NewLine;
        }

        /**********************************************************************************************//**
         * \fn  public override int getQuantity()
         *
         * \brief   Gets the quantity
         * \return  The quantity.
         **************************************************************************************************/
        public string getSinger() { return Singer; }
        public string getMCDType() { return MCD_Type; }
        public override int getStockCount() { return base.getStockCount(); }

        /**********************************************************************************************//**
         * \fn  public override void setQuantity(int X)
         *
         * \brief   Sets a quantity
         * \param   X   The X coordinate.
         **************************************************************************************************/

        public override void setStockCount(int X) { base.setStockCount(X); }

        /**********************************************************************************************//**
         * \fn  public override float getPrice()
         *
         * \brief   Gets the price
         * \return  The price.
         **************************************************************************************************/

        public override float getPrice() { return base.getPrice(); }

        /**********************************************************************************************//**
         * \fn  public override string getName()
         *
         * \brief   Gets the name
         * \return  The name.
         **************************************************************************************************/

        public override string getName() { return base.getName(); }

        /**********************************************************************************************//**
         * \fn  public override int getID()
         *
         * \brief   Gets the ıd
         * \return  The ıd.
         **************************************************************************************************/

        public override int getID() { return base.getID(); }

    }
}
