﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace _19_20_OOP2_Project.Classes
{
    /*! \class ItemToPurchase
     *  \brief It is ItemToPurchase class.
        \details It is used to hold product that customer wants to buy.
     */
    public class ItemToPurchase
    {
        private int _ProductID;
        private int _Quantity;
        public ItemToPurchase(int _ProductID,int _Quantity)
        {
            this._ProductID = _ProductID;
            this._Quantity = _Quantity;
        }
        public int returnProductID()
        {
            return _ProductID;
        }
        public int returnQuantity()
        {
            return _Quantity;
        }
    }
}
