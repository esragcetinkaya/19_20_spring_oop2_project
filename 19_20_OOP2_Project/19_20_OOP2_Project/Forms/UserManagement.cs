﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
namespace _19_20_OOP2_Project.Forms
{
    public partial class UserManagement : UserControl
    {
        List<Classes.Customer> listcollection = new List<Classes.Customer>();
        string[] type = { "Admin", "User", "Part-time User" }; 
        public UserManagement()
        {
            InitializeComponent();
            listcollection.Clear();
            foreach (Classes.Customer item in Forms.main.arrayUser)
            {
                listcollection.Add(item);
                listBoxUsers.Items.Add(string.Format("{0} | {1}", item.Username, item.Userrole));
                searchBox.CharacterCasing = CharacterCasing.Lower;
            }
            foreach (string item in type)
            {
                if (item.StartsWith(comboBoxUserRoles.Text))
                {
                    comboBoxUserRoles.Items.Add(item);
                }
            }
        }
        private void searchBox_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(searchBox.Text) == false)
            {
                listBoxUsers.Items.Clear();
                foreach (Classes.Customer item in listcollection)
                {
                    if (item.Username.StartsWith(searchBox.Text))
                    {
                        listBoxUsers.Items.Add(string.Format("{0}|{1}", item.Username, item.Userrole));
                    }
                }
            }
            else if (searchBox.Text == "")
            {
                listBoxUsers.Items.Clear();
                foreach (Classes.Customer item in listcollection)
                {
                    listBoxUsers.Items.Add(string.Format("{0} | {1}", item.Username, item.Userrole));
                }
            }
        }

        private void changeRole_Click(object sender, EventArgs e)
        {
            newlist.Items.Clear();
            string selecteditem = listBoxUsers.GetItemText(listBoxUsers.SelectedItem);
            string name = selecteditem.Split(' ')[0];
            string role = " ";
            foreach (Classes.Customer item in listcollection)
            {
                if (item.Username == name)
                {
                    role = item.Userrole;
                }
            }
            if (comboBoxUserRoles.SelectedItem == null)
            {
                MessageBox.Show("Please choose a user role!!");
            }
            else if (listBoxUsers.SelectedItem == null)
            {
                MessageBox.Show("Please choose a user!!");
            }
            else
            {
                if (role == comboBoxUserRoles.SelectedItem.ToString())
                {
                    MessageBox.Show("There is no change!!");
                }
                else
                {
                    try
                    {
                        foreach (Classes.Customer item in Forms.main.arrayUser)
                        {
                            List<string> lines = new List<string>();
                            using (StreamReader reader = new StreamReader(Classes.GetPathCsv.getPath("UserInformation")))
                            {
                                String line;
                                while ((line = reader.ReadLine()) != null)
                                {
                                    if (line.Contains(","))
                                    {
                                        String[] split = line.Split(',');
                                        if (split[0] == name && item.Username == name)
                                        {
                                            split[6] = comboBoxUserRoles.SelectedItem.ToString();
                                            line = String.Join(",", split);
                                            item.Userrole = comboBoxUserRoles.SelectedItem.ToString();
                                        }
                                    }
                                    lines.Add(line);
                                }
                            }
                            using (StreamWriter writer = new StreamWriter(Classes.GetPathCsv.getPath("UserManagement"), false))
                            {
                                foreach (String line in lines)
                                    writer.WriteLine(line);
                            }
                        }
                        foreach (Classes.Customer item in Forms.main.arrayUser)
                        {
                            newlist.Items.Add(string.Format("{0} | {1}", item.Username, item.Userrole));
                        }
                    }

                    catch
                    {
                        MessageBox.Show("User Role isn't changed!!");
                    }

                }
            }
        }
    }
}
