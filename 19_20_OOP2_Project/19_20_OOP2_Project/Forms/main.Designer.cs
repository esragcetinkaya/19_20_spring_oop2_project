﻿namespace _19_20_OOP2_Project.Forms
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.register_button = new System.Windows.Forms.Button();
            this.proMan = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.book_button = new System.Windows.Forms.Button();
            this.magazine_button = new System.Windows.Forms.Button();
            this.music_button = new System.Windows.Forms.Button();
            this.login_button = new System.Windows.Forms.Button();
            this.account_button = new System.Windows.Forms.Button();
            this.bucket_buttn = new System.Windows.Forms.Button();
            this.userMan = new System.Windows.Forms.Button();
            this.logout = new System.Windows.Forms.Button();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Teal;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.register_button);
            this.panel1.Location = new System.Drawing.Point(16, 15);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1473, 128);
            this.panel1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(532, 28);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(262, 52);
            this.label1.TabIndex = 0;
            this.label1.Text = "Signs of Life";
            this.label1.UseMnemonic = false;
            // 
            // register_button
            // 
            this.register_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.register_button.FlatAppearance.BorderSize = 0;
            this.register_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.register_button.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.register_button.ForeColor = System.Drawing.Color.White;
            this.register_button.Location = new System.Drawing.Point(1182, 33);
            this.register_button.Margin = new System.Windows.Forms.Padding(4);
            this.register_button.Name = "register_button";
            this.register_button.Size = new System.Drawing.Size(285, 62);
            this.register_button.TabIndex = 0;
            this.register_button.Text = "REGISTER";
            this.register_button.UseVisualStyleBackColor = true;
            this.register_button.Click += new System.EventHandler(this.register_button_Click);
            // 
            // proMan
            // 
            this.proMan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.proMan.FlatAppearance.BorderSize = 0;
            this.proMan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.proMan.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.proMan.ForeColor = System.Drawing.Color.White;
            this.proMan.Location = new System.Drawing.Point(4, 451);
            this.proMan.Margin = new System.Windows.Forms.Padding(4);
            this.proMan.Name = "proMan";
            this.proMan.Size = new System.Drawing.Size(285, 66);
            this.proMan.TabIndex = 7;
            this.proMan.Text = "Product Manager";
            this.proMan.UseVisualStyleBackColor = true;
            this.proMan.Click += new System.EventHandler(this.button1_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Teal;
            this.flowLayoutPanel1.Controls.Add(this.book_button);
            this.flowLayoutPanel1.Controls.Add(this.magazine_button);
            this.flowLayoutPanel1.Controls.Add(this.music_button);
            this.flowLayoutPanel1.Controls.Add(this.login_button);
            this.flowLayoutPanel1.Controls.Add(this.account_button);
            this.flowLayoutPanel1.Controls.Add(this.bucket_buttn);
            this.flowLayoutPanel1.Controls.Add(this.proMan);
            this.flowLayoutPanel1.Controls.Add(this.userMan);
            this.flowLayoutPanel1.Controls.Add(this.logout);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(16, 150);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(289, 698);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // book_button
            // 
            this.book_button.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.book_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.book_button.FlatAppearance.BorderSize = 0;
            this.book_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.book_button.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.book_button.ForeColor = System.Drawing.Color.White;
            this.book_button.Location = new System.Drawing.Point(4, 4);
            this.book_button.Margin = new System.Windows.Forms.Padding(4);
            this.book_button.Name = "book_button";
            this.book_button.Size = new System.Drawing.Size(285, 65);
            this.book_button.TabIndex = 1;
            this.book_button.Text = "BOOKS";
            this.book_button.UseVisualStyleBackColor = true;
            this.book_button.Click += new System.EventHandler(this.book_button_Click);
            // 
            // magazine_button
            // 
            this.magazine_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.magazine_button.FlatAppearance.BorderSize = 0;
            this.magazine_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.magazine_button.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.magazine_button.ForeColor = System.Drawing.Color.White;
            this.magazine_button.Location = new System.Drawing.Point(4, 77);
            this.magazine_button.Margin = new System.Windows.Forms.Padding(4);
            this.magazine_button.Name = "magazine_button";
            this.magazine_button.Size = new System.Drawing.Size(285, 64);
            this.magazine_button.TabIndex = 2;
            this.magazine_button.Text = "MAGAZINE";
            this.magazine_button.UseVisualStyleBackColor = true;
            this.magazine_button.Click += new System.EventHandler(this.magazine_button_Click);
            // 
            // music_button
            // 
            this.music_button.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.music_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.music_button.FlatAppearance.BorderSize = 0;
            this.music_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.music_button.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.music_button.ForeColor = System.Drawing.Color.White;
            this.music_button.Location = new System.Drawing.Point(4, 149);
            this.music_button.Margin = new System.Windows.Forms.Padding(4);
            this.music_button.Name = "music_button";
            this.music_button.Size = new System.Drawing.Size(285, 60);
            this.music_button.TabIndex = 3;
            this.music_button.Text = "MUSIC";
            this.music_button.UseVisualStyleBackColor = true;
            this.music_button.Click += new System.EventHandler(this.music_button_Click);
            // 
            // login_button
            // 
            this.login_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.login_button.FlatAppearance.BorderSize = 0;
            this.login_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.login_button.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.login_button.ForeColor = System.Drawing.Color.White;
            this.login_button.Location = new System.Drawing.Point(4, 217);
            this.login_button.Margin = new System.Windows.Forms.Padding(4);
            this.login_button.Name = "login_button";
            this.login_button.Size = new System.Drawing.Size(285, 68);
            this.login_button.TabIndex = 4;
            this.login_button.Text = "LOGIN";
            this.login_button.UseVisualStyleBackColor = true;
            this.login_button.Click += new System.EventHandler(this.login_button_Click);
            // 
            // account_button
            // 
            this.account_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.account_button.FlatAppearance.BorderSize = 0;
            this.account_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.account_button.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.account_button.ForeColor = System.Drawing.Color.White;
            this.account_button.Location = new System.Drawing.Point(4, 293);
            this.account_button.Margin = new System.Windows.Forms.Padding(4);
            this.account_button.Name = "account_button";
            this.account_button.Size = new System.Drawing.Size(285, 66);
            this.account_button.TabIndex = 5;
            this.account_button.Text = "ACCOUNT";
            this.account_button.UseVisualStyleBackColor = true;
            this.account_button.Click += new System.EventHandler(this.account_button_Click);
            // 
            // bucket_buttn
            // 
            this.bucket_buttn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bucket_buttn.FlatAppearance.BorderSize = 0;
            this.bucket_buttn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bucket_buttn.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.bucket_buttn.ForeColor = System.Drawing.Color.White;
            this.bucket_buttn.Location = new System.Drawing.Point(4, 367);
            this.bucket_buttn.Margin = new System.Windows.Forms.Padding(4);
            this.bucket_buttn.Name = "bucket_buttn";
            this.bucket_buttn.Size = new System.Drawing.Size(285, 76);
            this.bucket_buttn.TabIndex = 6;
            this.bucket_buttn.Text = "BUCKET";
            this.bucket_buttn.UseVisualStyleBackColor = true;
            this.bucket_buttn.Click += new System.EventHandler(this.bucket_buttn_Click);
            // 
            // userMan
            // 
            this.userMan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.userMan.FlatAppearance.BorderSize = 0;
            this.userMan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.userMan.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.userMan.ForeColor = System.Drawing.Color.White;
            this.userMan.Location = new System.Drawing.Point(4, 525);
            this.userMan.Margin = new System.Windows.Forms.Padding(4);
            this.userMan.Name = "userMan";
            this.userMan.Size = new System.Drawing.Size(285, 62);
            this.userMan.TabIndex = 8;
            this.userMan.Text = "User Management";
            this.userMan.UseVisualStyleBackColor = true;
            this.userMan.Click += new System.EventHandler(this.userMan_Click);
            // 
            // logout
            // 
            this.logout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.logout.FlatAppearance.BorderSize = 0;
            this.logout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.logout.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.logout.ForeColor = System.Drawing.Color.White;
            this.logout.Location = new System.Drawing.Point(4, 595);
            this.logout.Margin = new System.Windows.Forms.Padding(4);
            this.logout.Name = "logout";
            this.logout.Size = new System.Drawing.Size(285, 62);
            this.logout.TabIndex = 9;
            this.logout.Text = "LOG OUT";
            this.logout.UseVisualStyleBackColor = true;
            this.logout.Click += new System.EventHandler(this.logout_Click);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(315, 153);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1175, 694);
            this.flowLayoutPanel2.TabIndex = 4;
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1505, 862);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "main";
            this.Text = "main";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button bucket_buttn;
        private System.Windows.Forms.Button book_button;
        private System.Windows.Forms.Button music_button;
        private System.Windows.Forms.Button magazine_button;
        private System.Windows.Forms.Button account_button;
        private System.Windows.Forms.Button login_button;
        private System.Windows.Forms.Button register_button;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button userMan;
        private System.Windows.Forms.Button logout;
        private System.Windows.Forms.Button proMan;
    }
}