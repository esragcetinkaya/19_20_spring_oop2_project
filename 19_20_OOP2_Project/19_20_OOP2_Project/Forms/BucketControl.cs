﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _19_20_OOP2_Project.Classes;
using System.IO;
using Microsoft.Win32;
using System.Security.Cryptography.X509Certificates;
using System.Runtime.InteropServices.ComTypes;

namespace _19_20_OOP2_Project.Forms
{
    public partial class BucketControl : UserControl
    {
        //float price = 0;
        public List<Classes.Product> listOfProduct = new List<Product>();
        private Customer customer;
        public BucketControl(Customer _customer)
        {
            InitializeComponent();
            customer= _customer;
            textBox1.Text = _customer.ID;
        }
        private void remove_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                foreach (ShoppingCard item in main.ListOfShoppingCard)
                {
                    if (item.customer == customer.ID)
                    {
                        listBox1.Items.Remove(listBox1.SelectedItem);
                    }
                }
            }
            else
            {
                MessageBox.Show("Please select a product to remove!!");
            }
        }
        private void saveChanges_Click(object sender, EventArgs e)
        {
            if (radioButton2.Checked) {
                foreach(string item in listBox1.Items)
                {
                    string selectedItem = listBox1.GetItemText(listBox1.SelectedItem);
                    string name = selectedItem.Split(' ')[0];
                    string role = " ";
                }
            }
            else
            {

            }
        }
        private void BucketControl_Load(object sender, EventArgs e)
        {
            foreach (ShoppingCard item in main.ListOfShoppingCard)
            {
                if (item.customer==customer.ID)
                {
                    foreach(Product itemPro in main.ListOfProduct)
                    {
                        if (item.newItem.returnProductID() == itemPro.getID())
                        {
                            listBox1.Items.Add(string.Format("{0} | {1} | {2}",itemPro.getID(), itemPro.getName(),itemPro.getPrice()));
                           
                        }
                    }
                }
            }
        }
    }
}
