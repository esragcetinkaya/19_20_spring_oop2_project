﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _19_20_OOP2_Project.Classes;
using System.IO;
using System.Drawing.Imaging;
namespace _19_20_OOP2_Project.Forms
{
    public partial class EditBookUC : UserControl
    {
        //0=edit mode
        //1=create mode
        bool mode;
        Book temp;
        string path = "ProductData";

        public EditBookUC()
        {
            InitializeComponent();
            mode = false;
        }
        public EditBookUC(ListViewItem item)
        {
            InitializeComponent();

            textBoxID.Text = item.SubItems[0].Text;
            textBoxName.Text = item.SubItems[1].Text;
            textBoxPrice.Text = item.SubItems[2].Text;
            numericUpDown1.Value = Convert.ToDecimal(item.SubItems[3].Text);
            textBoxISBN.Text = item.SubItems[4].Text;
            textBoxAuthor.Text = item.SubItems[5].Text;
            textBoxPublisher.Text = item.SubItems[6].Text;
            numericUpDown2.Value = Convert.ToDecimal(item.SubItems[7].Text);
            byte[] imageBytes = Convert.FromBase64String(item.SubItems[8].Text);
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                pictureBox1.Image = Image.FromStream(ms, true);
            }
            temp = new Book(textBoxID.Text, textBoxName.Text, float.Parse(textBoxPrice.Text), Convert.ToInt32(numericUpDown1.Value), textBoxISBN.Text, textBoxAuthor.Text, textBoxPublisher.Text, Convert.ToInt32(numericUpDown2.Value));
            mode = true;
        }
        public void AddRecordToCSV()
        {
            Book book = new Book(textBoxID.Text, textBoxName.Text, float.Parse(textBoxPrice.Text), Convert.ToInt32(numericUpDown1.Value), textBoxISBN.Text, textBoxAuthor.Text, textBoxPublisher.Text, Convert.ToInt32(numericUpDown2.Value));
            var builder = new StringBuilder();
            var newLine = book.printCSV() + "," + storeTempImage();
            builder.AppendLine(newLine);
            File.AppendAllText(GetPathCsv.getPath(path), builder.ToString());
        }
        public void EditRecordCSV()
        {
            Book newBook = new Book(textBoxID.Text, textBoxName.Text, float.Parse(textBoxPrice.Text), Convert.ToInt32(numericUpDown1.Value), textBoxISBN.Text, textBoxAuthor.Text, textBoxPublisher.Text, Convert.ToInt32(numericUpDown2.Value));
            string[] rows = File.ReadAllLines(GetPathCsv.getPath(path));
            for (int i = 0; i < rows.Count(); i++)
            {
                string[] lines = rows[i].Split(',');
                if (lines[0] == "Book" && Convert.ToInt32(lines[1]) == temp.getID())
                {
                    rows[i] = newBook.printCSV() + "," + storeTempImage();
                }
            }
            File.WriteAllLines(GetPathCsv.getPath(path), rows);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (!mode)
                AddRecordToCSV();
            else
                EditRecordCSV();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            openFileDialog1.Title = "Please select an image to set as your profile picture";
            openFileDialog1.ShowHelp = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);
                byte[] imageArray = System.IO.File.ReadAllBytes(openFileDialog1.FileName);
                string base64ImageRepresentation = Convert.ToBase64String(imageArray);

            }
        }
        private string storeTempImage()
        {
            using (var memoryStream = new MemoryStream())
            {
                Bitmap bitmap = new Bitmap(pictureBox1.Image);
                bitmap.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                byte[] array = memoryStream.ToArray();
                string base64ImageRepresentation = Convert.ToBase64String(array);
                return base64ImageRepresentation;
            }
        }

        private void textBoxID_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textBoxPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                 (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
    }
}
