﻿namespace _19_20_OOP2_Project.Forms
{
    partial class Account
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_pass = new System.Windows.Forms.TextBox();
            this.password = new System.Windows.Forms.Label();
            this.txt_adress = new System.Windows.Forms.TextBox();
            this.email = new System.Windows.Forms.Label();
            this.UserName = new System.Windows.Forms.Label();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.saveChanges_button = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.Full_Name = new System.Windows.Forms.Label();
            this.txt_userName = new System.Windows.Forms.TextBox();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txt_pass
            // 
            this.txt_pass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_pass.Location = new System.Drawing.Point(264, 370);
            this.txt_pass.Margin = new System.Windows.Forms.Padding(4);
            this.txt_pass.Name = "txt_pass";
            this.txt_pass.PasswordChar = '*';
            this.txt_pass.Size = new System.Drawing.Size(546, 32);
            this.txt_pass.TabIndex = 17;
            this.txt_pass.UseSystemPasswordChar = true;
            // 
            // password
            // 
            this.password.AutoSize = true;
            this.password.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.password.Location = new System.Drawing.Point(88, 370);
            this.password.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(122, 30);
            this.password.TabIndex = 35;
            this.password.Text = "Password";
            // 
            // txt_adress
            // 
            this.txt_adress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_adress.Location = new System.Drawing.Point(264, 232);
            this.txt_adress.Margin = new System.Windows.Forms.Padding(4);
            this.txt_adress.Multiline = true;
            this.txt_adress.Name = "txt_adress";
            this.txt_adress.Size = new System.Drawing.Size(546, 117);
            this.txt_adress.TabIndex = 16;
            // 
            // email
            // 
            this.email.AutoSize = true;
            this.email.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.email.Location = new System.Drawing.Point(88, 169);
            this.email.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(78, 30);
            this.email.TabIndex = 33;
            this.email.Text = "Email";
            // 
            // UserName
            // 
            this.UserName.AutoSize = true;
            this.UserName.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.UserName.Location = new System.Drawing.Point(88, 112);
            this.UserName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.UserName.Name = "UserName";
            this.UserName.Size = new System.Drawing.Size(143, 30);
            this.UserName.TabIndex = 32;
            this.UserName.Text = "User Name";
            // 
            // txt_email
            // 
            this.txt_email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_email.Location = new System.Drawing.Point(264, 169);
            this.txt_email.Margin = new System.Windows.Forms.Padding(4);
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(546, 32);
            this.txt_email.TabIndex = 15;
            // 
            // saveChanges_button
            // 
            this.saveChanges_button.Location = new System.Drawing.Point(624, 446);
            this.saveChanges_button.Margin = new System.Windows.Forms.Padding(4);
            this.saveChanges_button.Name = "saveChanges_button";
            this.saveChanges_button.Size = new System.Drawing.Size(186, 62);
            this.saveChanges_button.TabIndex = 18;
            this.saveChanges_button.Text = "SAVE";
            this.saveChanges_button.UseVisualStyleBackColor = true;
            this.saveChanges_button.Click += new System.EventHandler(this.saveChanges_button_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(88, 232);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 30);
            this.label3.TabIndex = 29;
            this.label3.Text = "Addresses";
            // 
            // Full_Name
            // 
            this.Full_Name.AutoSize = true;
            this.Full_Name.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Full_Name.Location = new System.Drawing.Point(88, 56);
            this.Full_Name.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Full_Name.Name = "Full_Name";
            this.Full_Name.Size = new System.Drawing.Size(87, 30);
            this.Full_Name.TabIndex = 28;
            this.Full_Name.Text = "Name";
            // 
            // txt_userName
            // 
            this.txt_userName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_userName.Location = new System.Drawing.Point(264, 112);
            this.txt_userName.Margin = new System.Windows.Forms.Padding(4);
            this.txt_userName.Name = "txt_userName";
            this.txt_userName.Size = new System.Drawing.Size(546, 32);
            this.txt_userName.TabIndex = 14;
            // 
            // txt_name
            // 
            this.txt_name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_name.Location = new System.Drawing.Point(264, 58);
            this.txt_name.Margin = new System.Windows.Forms.Padding(4);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(546, 32);
            this.txt_name.TabIndex = 13;
            // 
            // Account
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.txt_pass);
            this.Controls.Add(this.password);
            this.Controls.Add(this.txt_adress);
            this.Controls.Add(this.email);
            this.Controls.Add(this.UserName);
            this.Controls.Add(this.txt_email);
            this.Controls.Add(this.saveChanges_button);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Full_Name);
            this.Controls.Add(this.txt_userName);
            this.Controls.Add(this.txt_name);
            this.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Account";
            this.Size = new System.Drawing.Size(899, 564);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_pass;
        private System.Windows.Forms.Label password;
        private System.Windows.Forms.TextBox txt_adress;
        private System.Windows.Forms.Label email;
        private System.Windows.Forms.Label UserName;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.Button saveChanges_button;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Full_Name;
        private System.Windows.Forms.TextBox txt_userName;
        private System.Windows.Forms.TextBox txt_name;
    }
}
