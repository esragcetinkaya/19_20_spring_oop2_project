﻿namespace _19_20_OOP2_Project.Forms
{
    partial class UserManagement
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.newlist = new System.Windows.Forms.ListBox();
            this.changeRole = new System.Windows.Forms.Button();
            this.comboBoxUserRoles = new System.Windows.Forms.ComboBox();
            this.searchLabel = new System.Windows.Forms.Label();
            this.searchBox = new System.Windows.Forms.TextBox();
            this.listBoxUsers = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // newlist
            // 
            this.newlist.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.newlist.FormattingEnabled = true;
            this.newlist.ItemHeight = 23;
            this.newlist.Location = new System.Drawing.Point(532, 146);
            this.newlist.Name = "newlist";
            this.newlist.Size = new System.Drawing.Size(268, 303);
            this.newlist.TabIndex = 28;
            // 
            // changeRole
            // 
            this.changeRole.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.changeRole.Location = new System.Drawing.Point(392, 297);
            this.changeRole.Name = "changeRole";
            this.changeRole.Size = new System.Drawing.Size(113, 42);
            this.changeRole.TabIndex = 27;
            this.changeRole.Text = "Change";
            this.changeRole.UseVisualStyleBackColor = true;
            this.changeRole.Click += new System.EventHandler(this.changeRole_Click);
            // 
            // comboBoxUserRoles
            // 
            this.comboBoxUserRoles.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.comboBoxUserRoles.FormattingEnabled = true;
            this.comboBoxUserRoles.Location = new System.Drawing.Point(377, 258);
            this.comboBoxUserRoles.Name = "comboBoxUserRoles";
            this.comboBoxUserRoles.Size = new System.Drawing.Size(149, 31);
            this.comboBoxUserRoles.TabIndex = 27;
            // 
            // searchLabel
            // 
            this.searchLabel.AutoSize = true;
            this.searchLabel.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.searchLabel.Location = new System.Drawing.Point(98, 116);
            this.searchLabel.Name = "searchLabel";
            this.searchLabel.Size = new System.Drawing.Size(78, 23);
            this.searchLabel.TabIndex = 25;
            this.searchLabel.Text = "Search";
            // 
            // searchBox
            // 
            this.searchBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.searchBox.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.searchBox.Location = new System.Drawing.Point(184, 115);
            this.searchBox.Multiline = true;
            this.searchBox.Name = "searchBox";
            this.searchBox.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.searchBox.Size = new System.Drawing.Size(186, 24);
            this.searchBox.TabIndex = 25;
            this.searchBox.TextChanged += new System.EventHandler(this.searchBox_TextChanged);
            // 
            // listBoxUsers
            // 
            this.listBoxUsers.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.listBoxUsers.FormattingEnabled = true;
            this.listBoxUsers.ItemHeight = 23;
            this.listBoxUsers.Location = new System.Drawing.Point(102, 146);
            this.listBoxUsers.Name = "listBoxUsers";
            this.listBoxUsers.Size = new System.Drawing.Size(268, 303);
            this.listBoxUsers.TabIndex = 26;
            // 
            // UserManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.newlist);
            this.Controls.Add(this.changeRole);
            this.Controls.Add(this.comboBoxUserRoles);
            this.Controls.Add(this.searchLabel);
            this.Controls.Add(this.searchBox);
            this.Controls.Add(this.listBoxUsers);
            this.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UserManagement";
            this.Size = new System.Drawing.Size(899, 564);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox newlist;
        private System.Windows.Forms.Button changeRole;
        private System.Windows.Forms.ComboBox comboBoxUserRoles;
        private System.Windows.Forms.Label searchLabel;
        private System.Windows.Forms.TextBox searchBox;
        private System.Windows.Forms.ListBox listBoxUsers;
    }
}
