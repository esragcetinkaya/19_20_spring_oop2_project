﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _19_20_OOP2_Project.Classes;
using System.IO;
using Microsoft.Win32;
using System.Security.Cryptography.X509Certificates;

namespace _19_20_OOP2_Project.Forms
{
    public partial class Login : UserControl
    {
        
        public Login()
        {
            InitializeComponent();
            UserName.Text = _19_20_OOP2_Project.Properties.Settings.Default.UserName;
            password.Text = _19_20_OOP2_Project.Properties.Settings.Default.Password;
        }
        private void login_button_Click_1(object sender, EventArgs e)
        {
            Boolean userNameContains = false;
            foreach (Classes.Customer item in main.arrayUser)
            {
                if (UserName.Text == item.Username)
                {
                    userNameContains = true;
                    if (item.Password == ComputeSha256Hash_.ComputeSha256Hash(password.Text))
                    {
                        if (checkBox1.Checked == true)
                        {
                            _19_20_OOP2_Project.Properties.Settings.Default.UserName = UserName.Text;
                            _19_20_OOP2_Project.Properties.Settings.Default.Password = password.Text;
                            _19_20_OOP2_Project.Properties.Settings.Default.Save();

                        }
                        MessageBox.Show("Success");
                        this.Parent.Parent.Hide();
                        main newmain = new main(item);
                        newmain.Show();
                    }
                    else
                    {
                        MessageBox.Show("Wrong Password");
                    }
                }
            }
            if (userNameContains == false)
            {
                MessageBox.Show("There is no user in this user name");
            }
        }
    }
}


