﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _19_20_OOP2_Project.Forms;
using System.IO;

namespace _19_20_OOP2_Project.Classes
{
    public partial class MagazineUserControl : UserControl
    {
        Customer _customer;
        Product _product;
        int quantity = 1;
        string data = "ShoppingCard";
        public MagazineUserControl(Customer _customer, Product _product)
        {
            InitializeComponent();
            this._customer = _customer;
            this._product = _product;
        }
        public MagazineUserControl( Product _product)
        {
            InitializeComponent();
            this.button1.Hide();
            this._product = _product;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ShoppingCard newde = new ShoppingCard(_customer.ID, "Cash", _product.getID(), Convert.ToInt32(numericUpDown1.Value));
            main.ListOfShoppingCard.Add(newde);
            StringWriter csv = new StringWriter();
            //textbox'daki girdileri dosyaya yazar.
            csv.WriteLine(string.Format("{0},{1},{2},{3}", _customer.ID, _product.getID(), "Cash", numericUpDown1.Value));
            File.AppendAllText(Classes.GetPathCsv.getPath(data), csv.ToString());
            StringWriter deneme = new StringWriter();
            deneme.WriteLine(string.Format("{0},{1},{2},{3}", _customer.ID, _product.getID(), "Cash", numericUpDown1.Value));
            File.AppendAllText(Classes.GetPathCsv.getPath(data), deneme.ToString());
        }
    }
}
