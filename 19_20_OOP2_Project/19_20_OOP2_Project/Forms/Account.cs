﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _19_20_OOP2_Project.Classes;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Text.RegularExpressions;
namespace _19_20_OOP2_Project.Forms
{
    public partial class Account : UserControl
    {
        internal Customer user;
        public Account(Customer _user)
        {
            InitializeComponent();
            user = _user;
            LoadInfo();
        }
        private void LoadInfo()  {
            txt_name.Text = user.Name;
            txt_adress.Text = user.Adress;
            txt_email.Text = user.Email;
            txt_userName.Text = user.Username;
        }
        private void saveChanges_button_Click(object sender, EventArgs e)
        {
            if (txt_userName.TextLength > 0 && txt_pass.TextLength > 0 && txt_name.TextLength > 0 && txt_adress.TextLength > 0 && txt_email.TextLength > 0 && Extension.IsValidEmail(txt_email.Text) )
            {
                string[] rows = File.ReadAllLines(GetPathCsv.getPath("UserInformation"));
                for (int i = 0; i < rows.Count(); i++)
                {
                    string[] lines = rows[i].Split(',');
                    if (lines[5] ==user.ID && lines[0]==user.Username){                    
                        lines[0] = txt_userName.Text;
                        lines[1] = ComputeSha256Hash_.ComputeSha256Hash(txt_pass.Text);
                        lines[2] = txt_name.Text;
                        lines[3] = txt_email.Text;
                        lines[4]= txt_adress.Text;
                        rows[i] = string.Join(",", lines);
                        MessageBox.Show("Success");
                        Customer cust = new Customer(lines[0], lines[1], lines[2], lines[3], lines[4], lines[5], lines[6]);
                        main.arrayUser.Remove(user);
                        main.arrayUser.Add(cust);
                        Account newde = new Account(cust);
                        this.Parent.Parent.Hide();
                        main newmain = new main(cust);            
                        newmain.Show();
                    }
                }
                File.WriteAllLines(GetPathCsv.getPath("UserInformation"), rows);
            }
            else if (txt_userName.TextLength > 0 && txt_pass.TextLength > 0 && txt_name.TextLength > 0 && txt_adress.TextLength > 0 && txt_email.TextLength > 0 && !Extension.IsValidEmail(txt_email.Text))
            {
                MessageBox.Show("Invalid email. Please enter a valid email. Example:email@email.com");
            }
            else
            {
                MessageBox.Show("You left one of the fields empty. Fill all fields and try again.");
            }
        }
    }
    
    public static class Extension
    {
        public static bool IsValidEmail(this string email)
        {
            string pattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|" + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)" + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";
            var regex = new Regex(pattern, RegexOptions.IgnoreCase);
            return regex.IsMatch(email);
        }
    }
}
