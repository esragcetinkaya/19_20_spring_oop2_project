﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using _19_20_OOP2_Project.Classes;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Mail;

namespace _19_20_OOP2_Project.Forms
{
    public partial class Register : UserControl
    {
        private string userRole = "user";
        public Register()
        {
            InitializeComponent();
        }
        public void AddUserToCSV(string path, Customer user)
        {
            var builder = new StringBuilder();
            string[] userinfo = user.printCustomerDetails();
            var newLine = string.Format("{0},{1},{2},{3},{4},{5},{6}", userinfo[0], userinfo[1], userinfo[2], userinfo[3], userinfo[4], userinfo[5], userinfo[6]);
            builder.AppendLine(newLine);
            File.AppendAllText(GetPathCsv.getPath(path), builder.ToString());
        }
        private void register_button_Click(object sender, EventArgs e)
        {
            Boolean a = false;
            foreach (Classes.Customer item in main.arrayUser)
            {
                if (txt_userName.Text == item.Username)
                {
                    a = true;
                }
            }
            if (txt_userName.TextLength > 0 && txt_pass.TextLength > 0 && txt_name.TextLength > 0 && txt_adress.TextLength > 0 && txt_email.TextLength > 0 && Extension.IsValidEmail(txt_email.Text) && a != true)
            {
                string path = "UserInformation";
                if (new FileInfo(GetPathCsv.getPath(path)).Length == 0) // if file is empty first user role is admin
                {
                    userRole = "admin";
                }
                Customer cust = new Customer(txt_userName.Text, ComputeSha256Hash_.ComputeSha256Hash(txt_pass.Text), txt_name.Text,  txt_email.Text, txt_adress.Text, "0", userRole);
                userRole = "user";
                string check = cust.ID;
                AddUserToCSV(path, cust);
                main.arrayUser.Add(cust);
                MessageBox.Show("Success");
                Login newde = new Login();
                this.Parent.Parent.Controls.Remove(register_button);
                this.Parent.Controls.Add(newde);
                this.Parent.Controls.Remove(this);
            }
            else if (a == true)
            {
                MessageBox.Show("There is a user in that name!!Please choose another user name!!");
            }
            else if (txt_userName.TextLength > 0 && txt_pass.TextLength > 0 && txt_name.TextLength > 0 && txt_adress.TextLength > 0 && txt_email.TextLength > 0 && !Extension.IsValidEmail(txt_email.Text))
            {
                MessageBox.Show("Invalid email. Please enter a valid email. Example:email@email.com");
            }
            else
            {
                MessageBox.Show("You left one of the fields empty. Fill all fields and try again.");
            }
        }

    }
}
