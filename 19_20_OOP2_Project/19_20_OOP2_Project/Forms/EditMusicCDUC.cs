﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using _19_20_OOP2_Project.Classes;

namespace _19_20_OOP2_Project.Forms
{
    public partial class EditMusicCDUC : UserControl
    {
        bool mode;
        MusicCD temp;
        string path = "ProductData";
        public EditMusicCDUC()
        {
            InitializeComponent();
            mode = false;
        }
        public EditMusicCDUC(ListViewItem item)
        {

            InitializeComponent();
            textBox1.Text = item.SubItems[0].Text;
            textBox2.Text = item.SubItems[1].Text;
            textBox3.Text = item.SubItems[2].Text;
            numericUpDown1.Value = Convert.ToDecimal(item.SubItems[3].Text);
            textBox6.Text = item.SubItems[4].Text;
            if (item.SubItems[5].Text == "Single")
            {
                comboBox1.SelectedIndex = 0;
            }
            else
                comboBox1.SelectedIndex = 1;
            byte[] imageBytes = Convert.FromBase64String(item.SubItems[6].Text);
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                pictureBox1.Image = Image.FromStream(ms, true);
            }
            temp = new MusicCD(Convert.ToInt32(textBox1.Text), textBox2.Text, float.Parse(textBox3.Text), Convert.ToInt32(numericUpDown1.Value), textBox6.Text, comboBox1.Text);
            mode = true;

        }
        private void addRecordCSV()
        {
            MusicCD cd = new MusicCD(Convert.ToInt32(textBox1.Text), textBox2.Text, float.Parse(textBox3.Text), Convert.ToInt32(numericUpDown1.Value), textBox6.Text, comboBox1.Text);
            var builder = new StringBuilder();
            var newLine = cd.printCSV() + "," + storeTempImage();
            builder.AppendLine(newLine);
            File.AppendAllText(GetPathCsv.getPath(path), builder.ToString());
        }
        private void EditMusicCDUC_Load(object sender, EventArgs e)
        {
        }
        private void EditMusicCDCSV()
        {
            MusicCD newCD = new MusicCD(Convert.ToInt32(textBox1.Text), textBox2.Text, float.Parse(textBox3.Text), Convert.ToInt32(numericUpDown1.Value), textBox6.Text, comboBox1.Text);
            string[] rows = File.ReadAllLines(GetPathCsv.getPath(path));
            for (int i = 0; i < rows.Count(); i++)
            {
                string[] lines = rows[i].Split(',');
                if (lines[0] == "Book" && Convert.ToInt32(lines[1]) == temp.getID())
                {
                    rows[i] = newCD.printCSV() + "," + storeTempImage();
                }
            }
            File.WriteAllLines(GetPathCsv.getPath(path), rows);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (mode == true)
                EditMusicCDCSV();
            else
                addRecordCSV();
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            openFileDialog1.Title = "Please select an image to set as your profile picture";
            openFileDialog1.ShowHelp = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);
            }
        }
        private string storeTempImage()
        {
            using (var memoryStream = new MemoryStream())
            {
                Bitmap bitmap = new Bitmap(pictureBox1.Image);
                bitmap.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                byte[] array = memoryStream.ToArray();
                string base64ImageRepresentation = Convert.ToBase64String(array);
                return base64ImageRepresentation;
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
    }
}
