﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using _19_20_OOP2_Project.Classes;

namespace _19_20_OOP2_Project.Forms
{
    public partial class ProductManager : UserControl
    {
        public ProductManager()
        {
            InitializeComponent();
        }
        //public ProductManager(Book book) { }
         private void button2_Click(object sender, EventArgs e)
        {
            //BookListView();

            if (listView1.SelectedItems.Count > 0)
            {
                if (comboBox1.SelectedIndex == 0)
                {
                    ListViewItem item = listView1.SelectedItems[0];
                    panel2.Controls.Clear();
                    EditBookUC ebuc = new EditBookUC(item);
                    panel2.Controls.Add(ebuc);
                }
                else if (comboBox1.SelectedIndex == 1)
                {
                    ListViewItem item = listView1.SelectedItems[0];
                    panel2.Controls.Clear();
                    EditMusicCDUC edmc = new EditMusicCDUC(item);
                    panel2.Controls.Add(edmc);

                }
                else
                {
                    ListViewItem item = listView1.SelectedItems[0];
                    panel2.Controls.Clear();
                    EditMagazineUC edmg = new EditMagazineUC(item);
                    panel2.Controls.Add(edmg);
                }
            }


        }
        private void BookListView()
        {

            // Create a new ListView control.
            //ListView listView1 = new ListView();
            listView1.Bounds = new Rectangle(new Point(0, 0), new Size(632, 237));

            // Set the view to show details.
            listView1.View = View.Details;
            // Allow the user to edit item text.
            listView1.LabelEdit = false;
            // Allow the user to rearrange columns.

            // Select the item and subitems when selection is made.
            listView1.FullRowSelect = true;
            // Display grid lines.
            listView1.GridLines = true;
            // Sort the items in the list in ascending order.


            // Create three items and three sets of subitems for each item.

            listView1.Columns.Add("ID", 100, HorizontalAlignment.Left);
            listView1.Columns.Add("Name", 130, HorizontalAlignment.Left);
            listView1.Columns.Add("Price", 70, HorizontalAlignment.Left);
            listView1.Columns.Add("Stock Count", 70, HorizontalAlignment.Center);
            listView1.Columns.Add("ISBN", 130, HorizontalAlignment.Left);
            listView1.Columns.Add("Author", 130, HorizontalAlignment.Left);
            listView1.Columns.Add("Publisher", 130, HorizontalAlignment.Left);
            listView1.Columns.Add("Page", 70, HorizontalAlignment.Left);

            string[] rows = File.ReadAllLines(GetPathCsv.getPath("Product"));
            for (int i = 0; i < rows.Count(); i++)
            {
                //defname,defsurname,defemail,defphone,defadress

                string[] lines = rows[i].Split(',');
                if (lines[2]=="Book")
                {
                    ListViewItem item = new ListViewItem(lines[1]);
                    for (int j = 2; j < lines.Length; j++)
                    {
                        item.SubItems.Add(lines[j]);
                    }
                    item.SubItems.Add(lines[0]);
                    listView1.Items.Add(item);
                }

            }

            // Place a check mark next to the item.


            // Create columns for the items and subitems.
            // Width of -2 indicates auto-size.


            panel1.Controls.Add(listView1);
        }
        private void MusicCDListView()
        {
            // Create a new ListView control.
            //ListView listView1 = new ListView();
            listView1.Bounds = new Rectangle(new Point(0, 0), new Size(632, 237));

            // Set the view to show details.
            listView1.View = View.Details;
            // Allow the user to edit item text.
            listView1.LabelEdit = false;
            // Allow the user to rearrange columns.

            // Select the item and subitems when selection is made.
            listView1.FullRowSelect = true;
            // Display grid lines.
            listView1.GridLines = true;
            // Sort the items in the list in ascending order.


            // Create three items and three sets of subitems for each item.


            // Create columns for the items and subitems.
            // Width of -2 indicates auto-size.
            listView1.Columns.Add("ID", 100, HorizontalAlignment.Left);
            listView1.Columns.Add("Name", 130, HorizontalAlignment.Left);
            listView1.Columns.Add("Price", 70, HorizontalAlignment.Left);
            listView1.Columns.Add("Stock", 70, HorizontalAlignment.Center);
            listView1.Columns.Add("Singer", 130, HorizontalAlignment.Left);
            listView1.Columns.Add("Type", 130, HorizontalAlignment.Left);

            string[] rows = File.ReadAllLines(GetPathCsv.getPath("Product"));
            for (int i = 0; i < rows.Count(); i++)
            {
                //defname,defsurname,defemail,defphone,defadress
                string[] lines = rows[i].Split(',');
                if (lines[2] == "MusicCD")
                {
                    ListViewItem item = new ListViewItem(lines[1]);
                    for (int j = 2; j < lines.Length; j++)
                    {
                        item.SubItems.Add(lines[j]);
                    }
                    item.SubItems.Add(lines[0]);
                    listView1.Items.Add(item);
                }

            }


            panel1.Controls.Add(listView1);
        }
        private void MagazineListView()
        {
            // Create a new ListView control.
            //ListView listView1 = new ListView();
            listView1.Bounds = new Rectangle(new Point(0, 0), new Size(632, 237));

            // Set the view to show details.
            listView1.View = View.Details;
            // Allow the user to edit item text.
            listView1.LabelEdit = false;
            // Allow the user to rearrange columns.

            // Select the item and subitems when selection is made.
            listView1.FullRowSelect = true;
            // Display grid lines.
            listView1.GridLines = true;
            // Sort the items in the list in ascending order.


            // Create three items and three sets of subitems for each item.


            // Create columns for the items and subitems.
            // Width of -2 indicates auto-size.
            listView1.Columns.Add("ID", 100, HorizontalAlignment.Left);
            listView1.Columns.Add("Name", 130, HorizontalAlignment.Left);
            listView1.Columns.Add("Price", 70, HorizontalAlignment.Left);
            listView1.Columns.Add("Stock", 70, HorizontalAlignment.Center);
            listView1.Columns.Add("Issue", 130, HorizontalAlignment.Left);


            string[] rows = File.ReadAllLines(GetPathCsv.getPath("Product"));
            for (int i = 0; i < rows.Count(); i++)
            {

                //defname,defsurname,defemail,defphone,defadress
                string[] lines = rows[i].Split(',');
                if (lines[2] == "Magazine")
                {
                    ListViewItem item = new ListViewItem(lines[1]);
                    for (int j = 2; j < lines.Length; j++)
                    {
                        item.SubItems.Add(lines[j]);
                    }
                    item.SubItems.Add(lines[0]);
                    listView1.Items.Add(item);
                }

            }

            panel1.Controls.Add(listView1);
        }


        private void ProductManager_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                listView1.Clear();
                BookListView();
                panel2.Controls.Clear();
                EditBookUC ebuc = new EditBookUC();
                panel2.Controls.Add(ebuc);
            }
            else if (comboBox1.SelectedIndex == 1)
            {
                listView1.Clear();
                MusicCDListView();
                panel2.Controls.Clear();
                EditMusicCDUC emuc = new EditMusicCDUC();
                panel2.Controls.Add(emuc);
            }
            else if (comboBox1.SelectedIndex == 2)
            {
                listView1.Clear();
                MagazineListView();
                panel2.Controls.Clear();
                EditMagazineUC emuc = new EditMagazineUC();
                panel2.Controls.Add(emuc);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BookListView();
            panel2.Controls.Clear();
            EditBookUC ebuc = new EditBookUC();
            panel2.Controls.Add(ebuc);
        }
        //Delete button
        private void button3_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                var rows = File.ReadAllLines(GetPathCsv.getPath("Product"));
                for (int rowIndex = 0; rowIndex != rows.Length; rowIndex++)
                {
                    string[] lines = rows[rowIndex].Split(',');
                    if (listView1.SelectedItems[0].SubItems[0].Text == lines[1] && listView1.SelectedItems[0].SubItems[1].Text == lines[2] && listView1.SelectedItems[0].SubItems[2].Text == lines[3])
                    {
                        rows = rows.Where(w => w != rows[rowIndex]).ToArray();
                        ListViewItem itm = listView1.SelectedItems[0];
                        listView1.Items[itm.Index].Remove();
                        break;
                    }
                }
                File.WriteAllLines(GetPathCsv.getPath("Product"), rows);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
