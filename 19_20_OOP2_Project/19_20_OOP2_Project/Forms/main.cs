﻿using _19_20_OOP2_Project.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Microsoft.Win32;
using System.Security.Cryptography.X509Certificates;

namespace _19_20_OOP2_Project.Forms
{
    public partial class main : Form
    {

        Customer a;
        internal static List<Customer> arrayUser = new List<Customer>();
        internal static List<Product> ListOfProduct = new List<Product>();
        internal static List<ShoppingCard> ListOfShoppingCard = new List<ShoppingCard>();
        public readonly string Userpath = "UserInformation";
        public readonly string Product = "Product";
        public readonly string ShoppingCard = "ShoppingCard";
        public main()
        {
            InitializeComponent();
            this.userMan.Hide();
            this.account_button.Hide();
            this.bucket_buttn.Hide();
            this.proMan.Hide();
            LoadUsers();
            Product_Load();
            loadLogin();
        }
        public main(Customer _a)
        {
            InitializeComponent();
            this.login_button.Hide();
            this.register_button.Hide();
            if (_a.Userrole != "admin")
            {
                this.userMan.Hide();
                this.proMan.Hide();
            }
            a = _a;
            Account newACC = new Account(a);
            flowLayoutPanel2.Controls.Add(newACC);
        }
        private void register_button_Click(object sender, EventArgs e)
        {
            flowLayoutPanel2.Controls.Clear();
            Register _register = new Register();
            flowLayoutPanel2.Controls.Add(_register);
        }
        private void login_button_Click(object sender, EventArgs e)
        {
            loadLogin();
        }
        private void account_button_Click(object sender, EventArgs e)
        {
            flowLayoutPanel2.Controls.Clear();
            Account _account = new Account(a);
            flowLayoutPanel2.Controls.Add(_account);
        }
        private void logout_Click(object sender, EventArgs e)
        {
            _19_20_OOP2_Project.Properties.Settings.Default.UserName = null;
            _19_20_OOP2_Project.Properties.Settings.Default.Password = null;
            _19_20_OOP2_Project.Properties.Settings.Default.Save();
            Application.Exit();
        }

        private void userMan_Click(object sender, EventArgs e)
        {
            flowLayoutPanel2.Controls.Clear();
            UserManagement _user = new UserManagement();
            flowLayoutPanel2.Controls.Add(_user);
        }

        private void bucket_buttn_Click(object sender, EventArgs e)
        {
            flowLayoutPanel2.Controls.Clear();
            BucketControl _bucket = new BucketControl(a);
            flowLayoutPanel2.Controls.Add(_bucket);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            flowLayoutPanel2.Controls.Clear();
            ProductManager _bucket = new ProductManager();
            flowLayoutPanel2.Controls.Add(_bucket);
        }
        private void loadLogin()
        {
            flowLayoutPanel2.Controls.Clear();
            Login _login = new Login();
            flowLayoutPanel2.Controls.Add(_login);
        }
        private void Main_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.UserClosing)
                return;
            if (MessageBox.Show("Are you sure you want to close the form?", "Warning", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                e.Cancel = true;
            else
            {
                e.Cancel = false;
            }
        }

        private void book_button_Click(object sender, EventArgs e)
        {
            flowLayoutPanel2.Controls.Clear();
            foreach (Product item in ListOfProduct)
            {
                if (a != null && item.getType()=="Book")
                {
                    BookUserControl newBook = new BookUserControl(a, item);
                    flowLayoutPanel2.Controls.Add(newBook);
                }
                else if(a==null && item.getType() == "Book")
                {
                    BookUserControl newBook = new BookUserControl(a, item);
                    flowLayoutPanel2.Controls.Add(newBook);
                }
            }           
        }
        private void Product_Load()
        {
            using (var reader = new StreamReader(GetPathCsv.getPath(Product)))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    Product newProduct = new Product(values[0],float.Parse(values[1]), values[2], int.Parse(values[3]));
                    main.ListOfProduct.Add(newProduct);

                }
            }
        }
        private void LoadUsers()
        {
            using (var reader = new StreamReader(GetPathCsv.getPath(Userpath)))

            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    Customer newuser = new Customer(values[0], values[1], values[2], values[3], values[4], values[5], values[6]);
                    main.arrayUser.Add(newuser);
                }
            }
        }
        private void LoadShoppingCard()
        {
            using (var reader = new StreamReader(GetPathCsv.getPath(ShoppingCard)))

            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    ShoppingCard newde = new ShoppingCard(values[0],values[1],int.Parse(values[2]),int.Parse(values[3]));
                    ListOfShoppingCard.Add(newde);
                }
            }
        }
        private void magazine_button_Click(object sender, EventArgs e)
        {
            flowLayoutPanel2.Controls.Clear();
            foreach (Product item in ListOfProduct)
            {
                if (a != null && item.getType() == "Magazine")
                {
                    MagazineUserControl newBook = new MagazineUserControl(a, item);
                    flowLayoutPanel2.Controls.Add(newBook);
                }
                else if (a == null && item.getType() == "Magazine")
                {
                    MagazineUserControl newBook = new MagazineUserControl(a, item);
                    flowLayoutPanel2.Controls.Add(newBook);
                }
            }
        }

        private void music_button_Click(object sender, EventArgs e)
        {
            flowLayoutPanel2.Controls.Clear();
            foreach (Product item in ListOfProduct)
            {
                if (a != null && item.getType() == "MusicCD")
                {
                    MusicCDUserControl newBook = new MusicCDUserControl(a, item);
                    flowLayoutPanel2.Controls.Add(newBook);
                }
                else if (a == null && item.getType() == "MusicCD")
                {
                    MusicCDUserControl newBook = new MusicCDUserControl(a, item);
                    flowLayoutPanel2.Controls.Add(newBook);
                }
            }
        }
    }
}

