﻿using _19_20_OOP2_Project.Classes;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace _19_20_OOP2_Project.Forms
{
    public partial class EditMagazineUC : UserControl
    {
        bool mode;
        Magazine temp;
        string path = "ProductData";
        public EditMagazineUC()
        {
            InitializeComponent();
            mode = false;
        }
        public EditMagazineUC(ListViewItem item)
        {
            InitializeComponent();
            textBoxID.Text = item.SubItems[0].Text;
            textBoxName.Text = item.SubItems[1].Text;
            textBoxPrice.Text = item.SubItems[2].Text;
            numericUpDown1.Value = Convert.ToDecimal(item.SubItems[3].Text);
            textBoxIssue.Text = item.SubItems[4].Text;
            byte[] imageBytes = Convert.FromBase64String(item.SubItems[5].Text);
            // Convert byte[] to Image
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                pictureBox1.Image = Image.FromStream(ms, true);
            }
            temp = new Magazine(Convert.ToInt32(textBoxID.Text), textBoxName.Text, float.Parse(textBoxPrice.Text), textBoxIssue.Text, Convert.ToInt32(numericUpDown1.Value));
            mode = true;

        }
        public void AddMagazineCSV()
        {
            Magazine magazine = new Magazine(Convert.ToInt32(textBoxID.Text), textBoxName.Text, float.Parse(textBoxPrice.Text), textBoxIssue.Text, Convert.ToInt32(numericUpDown1.Value));
            var builder = new StringBuilder();
            var newLine = magazine.printCSV() + "," + storeTempImage();
            builder.AppendLine(newLine);
            File.AppendAllText(GetPathCsv.getPath(path), builder.ToString());
        }
        public void EditMagazineCSV()
        {
            Magazine newMagazine = new Magazine(Convert.ToInt32(textBoxID.Text), textBoxName.Text, float.Parse(textBoxPrice.Text), textBoxIssue.Text, Convert.ToInt32(numericUpDown1.Value));
            string[] rows = File.ReadAllLines(GetPathCsv.getPath(path));
            for (int i = 0; i < rows.Count(); i++)
            {
                string[] lines = rows[i].Split(',');
                if (lines[0] == "Book" && Convert.ToInt32(lines[1]) == temp.getID())
                {
                    rows[i] = newMagazine.printCSV() + "," + storeTempImage();
                }
            }
            File.WriteAllLines(GetPathCsv.getPath(path), rows);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (mode == true)
                EditMagazineCSV();
            else
                AddMagazineCSV();
        }
        private string storeTempImage()
        {
            using (var memoryStream = new MemoryStream())
            {
                Bitmap bitmap = new Bitmap(pictureBox1.Image);
                bitmap.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                byte[] array = memoryStream.ToArray();
                string base64ImageRepresentation = Convert.ToBase64String(array);
                return base64ImageRepresentation;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            openFileDialog1.Title = "Please select an image to set as your profile picture";
            openFileDialog1.ShowHelp = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);

            }
        }

        private void textBoxID_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void button1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
    }

}
